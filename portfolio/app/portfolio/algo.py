def maxSum(arr, window):
    leng =  len(arr)
    if(window > leng):
        return -1
    winsum = sum(arr[i] for i in range(window))
    maxsum = winsum
    # print(maxsum)

    for i in range(leng - window):
        winsum = (winsum - arr[i]) + arr[window + i]
        maxsum = max(maxsum, winsum)
        # print(winsum)

    return maxsum

print(maxSum([2,5,5,5,1,2,3,4,3,1,7], 3))
