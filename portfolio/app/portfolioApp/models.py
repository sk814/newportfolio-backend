from django.db import models
from django.db.models.deletion import CASCADE
from django.contrib.postgres.fields import ArrayField

class Project(models.Model):
    associatedWith = models.CharField(max_length=50)
    projectType = models.CharField(max_length=30)

    def __str__(self) -> str:
        return self.associatedWith

class ProjectList(models.Model):
    associatedWith = models.ForeignKey(Project, related_name='association', on_delete=models.CASCADE)
    title = models.CharField(max_length=60)
    date = models.CharField(max_length=30)
    link = models.CharField(max_length=50,null=True, blank=True)
    description = ArrayField(models.CharField(max_length=200),null=True, blank=True)

    def __str__(self) -> str:
        return self.title
    
    class Meta:
        verbose_name = "projectList"
        verbose_name_plural = 'project list'

class ProjectImage(models.Model):
    project = models.ForeignKey(ProjectList, related_name='projectImage', default=None, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='projectImages', blank = True, null=True)

    def __str__(self):
        return self.project.title
    
    class Meta:
        verbose_name = "projectImages"
        verbose_name_plural = 'project images'


class MessagesReceived(models.Model):
    name = models.CharField(max_length=25)
    email = models.EmailField()
    messages = models.TextField()

    def __str__(self):
        return self.name