from django.contrib import admin
from portfolioApp.models import Project,ProjectList,MessagesReceived, ProjectImage

class ProjectListAdmin(admin.ModelAdmin):
    list_display = ('associatedWith', 'title')

class MessagesAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'messages')

# class ProduceGalleryInline(admin.TabularInline)

admin.site.register(Project)
admin.site.register(ProjectList, ProjectListAdmin)
admin.site.register(MessagesReceived,MessagesAdmin)
admin.site.register(ProjectImage)
