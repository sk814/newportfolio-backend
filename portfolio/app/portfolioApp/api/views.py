from rest_framework import generics
from portfolioApp.models import MessagesReceived, Project, ProjectList
from portfolioApp.api.serializer import ProjectSerializer, ProjectListSerializer, MessageSerializer

class ProjectView(generics.ListCreateAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

class ProjectDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

class ProjectListView(generics.ListCreateAPIView):
    queryset = ProjectList.objects.all()
    serializer_class = ProjectListSerializer

class MessagesView(generics.ListCreateAPIView):
    queryset = MessagesReceived.objects.all()
    serializer_class = MessageSerializer

class MessagesDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = MessagesReceived.objects.all()
    serializer_class = MessageSerializer