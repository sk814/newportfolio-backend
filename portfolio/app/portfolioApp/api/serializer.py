from rest_framework import serializers
from portfolioApp.models import MessagesReceived, Project, ProjectImage, ProjectList

class ProjectImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectImage
        fields = '__all__'
        
class ProjectListSerializer(serializers.ModelSerializer):
    projectImage = ProjectImageSerializer(read_only=True, many = True)
    class Meta:
        model = ProjectList
        fields = '__all__'

class ProjectSerializer(serializers.ModelSerializer):
    association = ProjectListSerializer(read_only=True, many = True)
    class Meta:
        model = Project
        fields = '__all__'

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = MessagesReceived
        fields = '__all__'
