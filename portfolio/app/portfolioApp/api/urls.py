from django import views
from django.contrib import admin
from django.urls import path
from portfolioApp.api import views

urlpatterns = [
    path('projects/', views.ProjectView.as_view()),
    path('projects/<pk>', views.ProjectDetailsView.as_view()),
    path('projectlist/', views.ProjectListView.as_view()),
    path('messages/', views.MessagesView.as_view()),
    path('messages/<pk>', views.MessagesDetailsView.as_view()),
]
