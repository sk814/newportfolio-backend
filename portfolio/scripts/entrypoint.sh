#!/bin/sh

# If error, just exit with exitcode 0
# User for debuging
set -e


# Cmd to run our application using uwsgi 
# --socket : run as a socket TCP on port 8000
# --master : run this as a master thread (in forground)
# --enable-threads : to enable multithreading
# --module app.wsgi : Entry point on our application to run with wsgi by djangi file.

uwsgi --socket :8000 --master --enable-threads --module portfolio.wsgi